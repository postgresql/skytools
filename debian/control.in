Source: skytools
Maintainer: Christoph Berg <myon@debian.org>
Section: database
Priority: optional
Standards-Version: 3.9.5
Build-Depends: python-all-dev, postgresql-server-dev-all (>= 119~),
 debhelper (>= 7.0.50~), xmlto, asciidoc
Homepage: http://wiki.postgresql.org/wiki/Skytools
Vcs-Git: git://anonscm.debian.org/pkg-postgresql/skytools.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-postgresql/skytools.git
XS-Testsuite: autopkgtest

Package: skytools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, python-psycopg2,
 skytools-modules
# /usr/share/skytools moved here
Replaces: skytools-modules-8.4 (<< 2.1.12)
Description: Database management tools for PostgreSQL
 Skytools is a set of PostgreSQL tools for WAL shipping, queueing and
 replication.
 .
 This package contains the Python parts of skytools:
  - londiste: PostgreSQL replication engine written
    Python, using PgQ as event transport
  - pgqadm: PgQ queueing administration interface
  - walmgr: Managing WAL-based replication

Package: skytools-modules-PGVERSION
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, postgresql-PGVERSION
Provides: skytools-modules
Conflicts: postgresql-PGVERSION-pgq3
Replaces: postgresql-PGVERSION-pgq3
Description: PostgreSQL PGVERSION modules for skytools
 Skytools is a set of PostgreSQL tools for WAL shipping, queueing and
 replication.
 .
 This package contains the PostgreSQL modules for skytools:
  - txid: Provides 8-byte transaction ids for external usage
  - logtriga: Trigger function for table event logging in "partial SQL"
    format, used in londsite for replication
  - logutriga: Trigger function for table event logging in
    urlencoded format
  - londiste: Database parts of replication engine
  - pgq: Generic queue in database
